﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceNeeds
{
    public Resource resource;
    public int numberOfResources;

}
