﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Event", menuName = "WandererProject/Event")]
public class CardEvent : ScriptableObject
{
    public string eventName;
    [TextArea]
    public string description;
    public List<EventOption> eventOptions;
    public Texture2D icon;
    public bool sequenceEvent;
    [Range(0,100)]
    public int probability;

}
