﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionCard : MonoBehaviour
{
    public GameObject imageObj;
    public TextMeshProUGUI textObj;
    public GameObject resourceIconPrefab;
    public GameObject costBox;
    public GameObject rewardBox;
    public TextMeshProUGUI conditionText;

    public EventOption option;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void UpdateVisuals(EventOption _option)
    {
        option = _option;

        //imageObj.GetComponent<RawImage>().texture = option.icon;
        textObj.text = option.description;
        conditionText.text = option.consequenceText;

        //cost
        foreach (ResourceNeeds rn in _option.costs)
        {
            for(int i =0;i<rn.numberOfResources;i++)
            {
                GameObject newCost = Instantiate(resourceIconPrefab, costBox.transform);
                newCost.GetComponent<RawImage>().texture = rn.resource.littleIcon;
            }
            
        }

        //reward
        foreach (ResourceNeeds rn in _option.rewards)
        {
            for (int i = 0; i < rn.numberOfResources; i++)
            {
                GameObject newCost = Instantiate(resourceIconPrefab, rewardBox.transform);
                newCost.GetComponent<RawImage>().texture = rn.resource.littleIcon;
            }
        }

    }

    public void UnavailableOption()
    {
        imageObj.GetComponent<Button>().interactable = false;
    }



    public void PickThisChoice()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        manager.GetComponent<UIManager>().OptionSelectedAnimations(this.gameObject,option);
    }
}
