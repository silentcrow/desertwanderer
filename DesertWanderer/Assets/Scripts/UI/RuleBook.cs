﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuleBook : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenRuleBook()
    {
        GetComponent<Animator>().SetBool("PickUpBook", true);
    }

    public void CloseRuleBook()
    {
        GetComponent<Animator>().SetBool("PickUpBook", false);
    }
}
