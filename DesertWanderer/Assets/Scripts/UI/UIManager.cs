﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [Header("Card Event")]
    public GameObject cardEventHolder;
    public GameObject cardEventImage;
    public TextMeshProUGUI cardEventText;
    public TextMeshProUGUI cardEventName;

    [Header("Resources")]
    public GameObject resourcePrefab;
    public GameObject resourceBox;

    [Header("Options")]
    public GameObject optionsPrefab;
    public GameObject optionsBox;

    [Header("Pebles")]
    public GameObject peblePrefab;
    public GameObject pebleBox;

    [Header("Other")]
    public TextMeshProUGUI resourceCount;

    [Header("WinLose")]
    public GameObject losePanel;
    public GameObject winPanel;
    public GameObject fadePanel;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateCardEventUI(CardEvent _cardEvent)
    {
        cardEventImage.GetComponent<RawImage>().texture = _cardEvent.icon;
        cardEventName.text = _cardEvent.eventName;
        cardEventText.text = _cardEvent.description;
        cardEventImage.transform.parent.gameObject.SetActive(true);
        StartCoroutine(UpdateCardIE(_cardEvent));
       
    }

    IEnumerator UpdateCardIE(CardEvent _cardEvent)
    {
        yield return new WaitForSeconds(1.5f);
        UpdateOptions(_cardEvent);
    }

    private void UpdateOptions(CardEvent _cardEvent)
    {
        //clear previous options
        foreach (Transform child in optionsBox.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        //load new options

        StartCoroutine(UpdateOptionsIEP(_cardEvent)); 


    }

    IEnumerator UpdateOptionsIEP (CardEvent _cardEvent)
    {

        //clear current resource of leftouts
        foreach (GameObject c in GetComponent<WorldManager>().currentResources)
        {
            if (c == null)
            {
                Debug.Log("FOUND A LEFT OUT");
                GetComponent<WorldManager>().currentResources.Remove(c);
            }
        }


        for (int i = 0; i < _cardEvent.eventOptions.Count; i++)
        {
            GameObject newOption = Instantiate(optionsPrefab, optionsBox.transform);
            newOption.transform.GetChild(0).GetComponent<OptionCard>().UpdateVisuals(_cardEvent.eventOptions[i]);
            //move into board animation
            yield return new WaitForSeconds(0.5f);
            


            if (!ValidateOption(_cardEvent.eventOptions[i]))
            {
                newOption.transform.GetChild(0).GetComponent<OptionCard>().UnavailableOption();
            }
        }
        
    }

    public void UpdatePebles()
    {
        StartCoroutine(UpdatePeblesIE());
    }

    IEnumerator UpdatePeblesIE()
    {
        
        List<GameObject> deleteAfter = new List<GameObject>();


       // Debug.Log("current pebles: " + (15 - pebleBox.transform.childCount).ToString());
       // Debug.Log("current resources: " + (GetComponent<WorldManager>().currentResources.Count).ToString());

        int difference = (15 - pebleBox.transform.childCount) - GetComponent<WorldManager>().currentResources.Count;
        //Debug.Log("diference: " + difference);
        //Add
        if (difference > 0)
        {
           // Debug.Log("should add a peblle");
            //add pebles
            for (int i = 0; i <difference; i++)
            {
               // Debug.Log("added a peblle");
                if(pebleBox.transform.childCount<15)
                {
                    Instantiate(peblePrefab, pebleBox.transform);
                    yield return new WaitForSeconds(0.2f);
                }
                
            }
        }
        //Remove
        else if(difference < 0)
        {
            for (int i = 0; i < -difference; i++)
            {

                if (pebleBox.transform.childCount -1-i>=0)
                {
                    Debug.Log("child pos : " + (pebleBox.transform.childCount - 1 - i).ToString());
                    //  Debug.Log("removed a peblle");
                    pebleBox.transform.GetChild(pebleBox.transform.childCount - 1 - i).GetChild(0).GetComponent<Animator>().SetTrigger("Remove");
                    yield return new WaitForSeconds(0.2f);
                    deleteAfter.Add(pebleBox.transform.GetChild(pebleBox.transform.childCount - 1 - i).gameObject);
                }
               

            }
            yield return new WaitForSeconds(1f);

            foreach (GameObject g in deleteAfter)
            {
                Destroy(g);
            }


           
        }


    }

    private bool ValidateOption(EventOption _option)
    {
        if (_option.costs.Count != 0)
        {
            foreach (ResourceNeeds rn in _option.costs)
            {
                int quantityAvailable = 0;

                foreach (GameObject g in GetComponent<WorldManager>().currentResources)
                {
                    if (g.gameObject.transform.GetChild(0).GetComponent<ResourceCard>().resorceType == rn.resource)
                    {
                        quantityAvailable += 1;
                    }
                }
                if (quantityAvailable < rn.numberOfResources)
                {
                    return false;
                }
            }
        }


        return true;
    }


    public void AddResource(Resource _resouce)
    {
        if (resourceBox.transform.childCount == 0)//check if there are no resources still
        {
            //Debug.Log("added resource " + _resouce.name);
            GameObject newResource = Instantiate(resourcePrefab, resourceBox.transform);
            newResource.transform.GetChild(0).GetComponent<ResourceCard>().UpdateVisuals(_resouce);
            GetComponent<WorldManager>().currentResources.Add(newResource);
        }
        else
        {
            int lastplace = resourceBox.transform.childCount;
            for (int i = 0; i < resourceBox.transform.childCount; i++)
            {
                //gets the last place where there is a resource equal to the new one
                if (resourceBox.transform.GetChild(i).transform.GetChild(0).GetComponent<ResourceCard>().resorceType == _resouce)
                {
                    lastplace = i;
                }
            }

            GameObject newResource = Instantiate(resourcePrefab, resourceBox.transform);
            newResource.transform.GetChild(0).GetComponent<ResourceCard>().UpdateVisuals(_resouce);
            newResource.transform.SetSiblingIndex(lastplace);
           // Debug.Log("added resource "+ _resouce.name);
            GetComponent<WorldManager>().currentResources.Add(newResource);
            UpdateResourceCount();
        }

       // StartCoroutine(UpdatePebles());
    }


    public void RemoveResource(Resource _resouce,int _num)
    {

        StartCoroutine(RemoveResourceIE(_resouce, _num));
        //StartCoroutine(UpdatePebles());
    }

    IEnumerator RemoveResourceIE(Resource _resouce, int _num)
    {
        List<GameObject> deleteLater = new List<GameObject>();

        int lastplace = 0;
        //Find the lastplace
        for (int i = 0; i < resourceBox.transform.childCount; i++)
        {
            //gets the last place where there is a resource equal to the new one
            if (resourceBox.transform.GetChild(i).transform.GetChild(0).GetComponent<ResourceCard>().resorceType == _resouce)
            {
                lastplace = i;
                Debug.Log("Last place at : " + lastplace);
            }
        }

        for (int j = 0; j < _num; j++)
        {

            Debug.Log("removing at : " + (lastplace - j));
            resourceBox.transform.GetChild(lastplace - j).gameObject.transform.GetChild(0).GetComponent<ResourceCard>().RemoveFromGame();
            yield return new WaitForSeconds(0.2f);

            //Debug.Log("remove resource" + _resouce.name);
            /*
            GetComponent<WorldManager>().currentResources.Remove(resourceBox.transform.GetChild(lastplace-j).gameObject); //remove from currentresourceList
            resourceBox.transform.GetChild(lastplace - j).gameObject.transform.GetChild(0).GetComponent<Animator>().SetTrigger("ExitBoard");//start exit animation
            yield return new WaitForSeconds(0.2f);
            deleteLater.Add(resourceBox.transform.GetChild(lastplace - j).gameObject);

    */

        }

        /*
        yield return new WaitForSeconds(1f);
        foreach (GameObject g in deleteLater)
        {
            Destroy(g);
            //Debug.Log("Destroy resouce " + _resouce.name);
        }
        */

 
        
    }

    private void UpdateResourceCount()
    {
        resourceCount.text = resourceBox.transform.childCount.ToString();
    }

    public void OptionSelectedAnimations(GameObject _selectedOption,EventOption _option)
    {

        GameObject seletectedOption = null;
        int pos=0;
        for (int i = 0; i < optionsBox.transform.childCount; i++)
        {

            if (optionsBox.transform.GetChild(i).transform.GetChild(0).GetComponent<OptionCard>().option != _selectedOption.GetComponent<OptionCard>().option)
            {
                optionsBox.transform.GetChild(i).transform.GetChild(0).GetComponent<Animator>().SetTrigger("Fade");
            }
            else
            {
                pos = i;
                seletectedOption = _selectedOption;
                             
            }
            
        }
        StartCoroutine(OptionSelectedIE(seletectedOption,pos,_option));

    }

    IEnumerator OptionSelectedIE(GameObject _selectedOption,int _position,EventOption _option)
    {
        yield return new WaitForSeconds(1.5f);
        _selectedOption.GetComponent<Animator>().SetInteger("Position", _position);
        _selectedOption.GetComponent<Animator>().SetTrigger("Selected");

        if(_option.winCondition)
        {
            StartCoroutine(WinConditionMeet(_option));
        }
        else if(_option.loseCondition)
        {
            StartCoroutine(LoseConditionMeet(_option));
        }
        else
        {
            yield return new WaitForSeconds(1f);
            GetComponent<WorldManager>().PickChoice(_option.costs, _option.rewards, _option.concequences);
            yield return new WaitForSeconds(1f);
            _selectedOption.GetComponent<Animator>().SetTrigger("SelectedLeave");
            GetComponent<WorldManager>().NextEvent();
        }

       
    }

    IEnumerator WinConditionMeet(EventOption _option)
    {
        foreach (ResourceNeeds r in _option.costs)
        {
            GetComponent<WorldManager>().RemoveResource(r.numberOfResources, r.resource);
        }
        yield return new WaitForSeconds(1f);
        winPanel.SetActive(true);
    }

    IEnumerator LoseConditionMeet(EventOption _option)
    {
        yield return new WaitForSeconds(1f);
        losePanel.SetActive(true);
    }




    public void ButtonPlayAgain()
    {
        StartCoroutine(ButtonNextSceneIE("GameScene"));
    }  

    public void ButtonBackToMenu()
    {
        StartCoroutine(ButtonNextSceneIE("MainMenu"));
    }

    IEnumerator ButtonNextSceneIE(string _nextScene)
    {
        fadePanel.GetComponent<Animator>().SetTrigger("Fade");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(_nextScene);
    }
}

