﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceCard : MonoBehaviour
{
    public GameObject imageObj;
    public Resource resorceType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateVisuals(Resource _resource)
    {
        resorceType = _resource;
        imageObj.GetComponent<RawImage>().texture = resorceType.icon;
    }

    public void RemoveFromGame()
    {
        StartCoroutine(RemoveFromGameIE());
    }

    IEnumerator RemoveFromGameIE()
    {

        GameObject.FindGameObjectWithTag("Manager").GetComponent<WorldManager>().currentResources.Remove(transform.parent.gameObject); //remove from currentresourceList
        GetComponent<Animator>().SetTrigger("ExitBoard");//start exit animation
        yield return new WaitForSeconds(1f);
        Destroy(transform.parent.gameObject);
        //deleteLater.Add(resourceBox.transform.GetChild(lastplace - j).gameObject);
    }
}
