﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventOption
{

    public string description;//text that apears to the player
    public Texture2D icon;
    public List<ResourceNeeds> costs; //cost
    public List<ResourceNeeds> rewards; //reward
    public List<CardEvent> concequences;
    public bool winCondition;
    public bool loseCondition;
    public string consequenceText;



}
