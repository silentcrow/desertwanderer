﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Option", menuName = "WandererProject/Resource")]
public class Resource : ScriptableObject
{
    public string nameOfResource;
    public Texture2D icon;
    public Texture2D littleIcon;

}
