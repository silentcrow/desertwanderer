﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour
{

    [Header("General")]
    public List<CardEvent> deck;
    private List<CardEvent> usedDeck;
    public List<GameObject> currentResources;
    public int dayToReshuffle;

    [Header("Info")]
    public List<CardEvent> fullDeck;
    public int maxNumOfEventOnDeck;
    public List<CardEvent> beginingEvents;
    public List<Resource> resourceList;
    public CardEvent currentEvent;


    [Header("Triggered EventCards")]
    public CardEvent badRepEvent;
    public Resource badRepResource;
    public CardEvent lackOfFoodEvent;
    public Resource foodResource;
    public CardEvent lackOfStaminaEvent;
    public Resource staminaResource;
    public CardEvent lackOfSanityEvent;
    public Resource sanityResource;
    public CardEvent overloadEvent;


    public int numOfEvents = 0;
    private List<GameObject> highlightedResources;
    // Start is called before the first frame update
    void Start()
    {
        highlightedResources = new List<GameObject>();

        StartGame();


    }

    // Update is called once per frame
    void Update()
    {
       
    }


    void ShuffleDeck()
    {
        if(numOfEvents>=dayToReshuffle)
        {
            //keep all hazords and opportonities currently in the deck
            List<CardEvent> hazordsAndOpporDeck = new List<CardEvent>() ;
            foreach(CardEvent c in deck)
            {
                if(c.probability!=0)
                {
                    hazordsAndOpporDeck.Add(c);
                }
            }

            deck.Clear();
            
            foreach(CardEvent c in hazordsAndOpporDeck)
            {
                deck.Add(c);
            }

            //pick random cardevents and add to deck
            List<CardEvent> fullDeckCopy = new List<CardEvent>();
            foreach (CardEvent c in fullDeck)
            {
                fullDeckCopy.Add(c);
            }

            for (int i = 0; i < maxNumOfEventOnDeck; i++)
            {
                int j = Random.Range(0, fullDeckCopy.Count);
                deck.Add(fullDeckCopy[j]);
                fullDeckCopy.RemoveAt(j);
            }


        }
    }

    void CheckTriggers()
    {
        //overweight
        if (currentResources.Count >= 15 && !deck.Contains(overloadEvent))
        {
            Debug.Log("overweight event added");
            deck.Add(overloadEvent);
        }

        if (numOfEvents>=5)
        {
           

            //Bad reputation
            if(CountResourceType(badRepResource)>=5 && !deck.Contains(badRepEvent))
            {
                Debug.Log("Bad reputation event added");
                deck.Add(badRepEvent);
            }

            //Starvation
            if (CountResourceType(foodResource) == 0 && !deck.Contains(lackOfFoodEvent))
            {
                Debug.Log("Starvation event added");
                deck.Add(lackOfFoodEvent);
            }

            //Fatigue
            if (CountResourceType(staminaResource) == 0 && !deck.Contains(lackOfStaminaEvent))
            {
                Debug.Log("Fatigue event added");
                deck.Add(lackOfStaminaEvent);
            }

            //Insanity
            if (CountResourceType(sanityResource) == 0 && !deck.Contains(lackOfSanityEvent))
            {
                Debug.Log("Insanity event added");
                deck.Add(lackOfSanityEvent);
            }
        }
    }

    int CountResourceType(Resource _resourceType)
    {
        int count = 0;
        foreach(GameObject g in currentResources)
        {
            if(g.transform.GetChild(0).GetComponent<ResourceCard>().resorceType == _resourceType)
            {
                count += 1;
            }
        }
        Debug.Log(_resourceType.ToString() + " at " + count);
        return count;
    }


    private void StartGame()
    {
        //pick a random event from the start event
        int rand = Random.Range(0, beginingEvents.Count);
        currentEvent = beginingEvents[rand];

        //pick random cardevents and add to deck
        List<CardEvent> fullDeckCopy = new List<CardEvent>();
        foreach(CardEvent c in fullDeck)
        {
            fullDeckCopy.Add(c);
        }

        for (int i = 0; i < maxNumOfEventOnDeck; i++)
        {
            int j = Random.Range(0, fullDeckCopy.Count);
            deck.Add(fullDeckCopy[j]);
            fullDeckCopy.RemoveAt(j);
        }



        //update visuals
        GetComponent<UIManager>().UpdateCardEventUI(currentEvent);

        //update pebles
        GetComponent<UIManager>().UpdatePebles();
    }



    public void RemoveResource(int _num, Resource _resource)
    {
        /*
        List<Resource> resourceOfRightType = new List<Resource>();
        
        foreach(Resource r in resourceList)
        {
            if(r == _resource)
            {
                resourceOfRightType.Add(r);
            }
        }

        for(int i =0;i<_num;i++)
        {
            resourceList.Remove(resourceOfRightType[i]);
           
        }
        */
        GetComponent<UIManager>().RemoveResource(_resource,_num);

    }



    private void AddResource(int _num, Resource _resource)
    {

        //StartCoroutine(AddResourceIE(_num,_resource));
        for (int i = 0; i < _num; i++)
        {
            resourceList.Add(_resource);
            GetComponent<UIManager>().AddResource(_resource);

        }

    }

    IEnumerator AddResourceIE(int _num, Resource _resource)
    {

        for (int i = 0; i < _num; i++)
        {
            resourceList.Add(_resource);           
            GetComponent<UIManager>().AddResource(_resource);
            
        }
        yield return new WaitForSeconds(0.5f);
        GetComponent<UIManager>().UpdatePebles();
        CheckTriggers();

    }



    public void RemoveEvent(CardEvent oldEvent)
    {
        if(!oldEvent.sequenceEvent)
        {
            usedDeck.Add(oldEvent);
        }
        
        deck.Remove(oldEvent);
    }

    public void AddEvent(CardEvent newEvent)
    {
        deck.Add(newEvent);
    }


    public void PickChoice(List<ResourceNeeds> _cost, List<ResourceNeeds> _reward,List<CardEvent> _consequence)
    {
        ShuffleDeck();
        foreach (ResourceNeeds r in _cost)
        {
            RemoveResource(r.numberOfResources, r.resource);
        }

        foreach (ResourceNeeds r in _reward)
        {
            AddResource(r.numberOfResources, r.resource);
        }


        StartCoroutine(UpdatePeblesIE());


        foreach (CardEvent c in _consequence)
        {
            AddEvent(c);
        }

        // currentEvent = NewEvent();
        //GetComponent<UIManager>().UpdateCardEventUI(currentEvent);
      
    }

    IEnumerator UpdatePeblesIE()
    {
        yield return new WaitForSeconds(1.5f);
        GetComponent<UIManager>().UpdatePebles();
        numOfEvents += 1;
        CheckTriggers();
    }


    public void NextEvent()
    {
        StartCoroutine(NewEventIE());
    }

    IEnumerator NewEventIE()
    {
        GetComponent<UIManager>().cardEventHolder.GetComponent<Animator>().SetTrigger("ExitGame");
        yield return new WaitForSeconds(1f);
        GetComponent<UIManager>().cardEventHolder.GetComponent<Animator>().SetTrigger("StartGame");
        currentEvent = NewEvent();

        GetComponent<UIManager>().UpdateCardEventUI(currentEvent);
    }

    public CardEvent NewEvent()
    {
        if(deck.Contains(currentEvent))
        {
            deck.Remove(currentEvent);
        }
       

        //finds out if there is a follow up event
        foreach(CardEvent c in deck)
        {
            if(c.probability == 100)
            {
                return c;
            }
        }

        //runs probability events
        int rand = Random.Range(0, 101);
        List<CardEvent> probabilityEvents = new List<CardEvent>();
        foreach( CardEvent c in deck)
        {
            if(rand >(100-c.probability))
            {
                probabilityEvents.Add(c);
            }
        }

        //checks if there is any probability events found
        if(probabilityEvents.Count!=0)
        {
            int i = Random.Range(0, probabilityEvents.Count);
            return probabilityEvents[i];
        }
        else//picks a random event from the deck
        {
            int i = Random.Range(0, deck.Count);
            return deck[i];
        }


    }


   
}
